CREATE DATABASE  IF NOT EXISTS `mun_sanborja` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `mun_sanborja`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: mun_sanborja
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ml_cotizacion_detalle`
--

DROP TABLE IF EXISTS `ml_cotizacion_detalle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ml_cotizacion_detalle` (
  `id_cotizacion_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `id_cotizacion` int(11) NOT NULL,
  `id_pedido_detalle` int(11) NOT NULL,
  `cantidad` double NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `valor_venta` decimal(10,0) NOT NULL,
  PRIMARY KEY (`id_cotizacion_detalle`),
  KEY `fk_cotizacion_detalle_idx` (`id_cotizacion`),
  KEY `fk_pedido_cotizacion_detalle_idx` (`id_pedido_detalle`),
  CONSTRAINT `fk_cotizacion_detalle` FOREIGN KEY (`id_cotizacion`) REFERENCES `mli_cotizacion` (`id_cotizacion`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_pedido_cotizacion_detalle` FOREIGN KEY (`id_pedido_detalle`) REFERENCES `mli_pedido_detalle` (`id_detallepedido`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ml_cotizacion_detalle`
--

LOCK TABLES `ml_cotizacion_detalle` WRITE;
/*!40000 ALTER TABLE `ml_cotizacion_detalle` DISABLE KEYS */;
INSERT INTO `ml_cotizacion_detalle` VALUES (1,1,1,10,'Descripción Cotización 1',3),(2,2,1,10,'Descripción Cotización 2',4),(3,3,1,10,'Descripción Cotización 2',5);
/*!40000 ALTER TABLE `ml_cotizacion_detalle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mli_area`
--

DROP TABLE IF EXISTS `mli_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mli_area` (
  `id_area` int(11) NOT NULL,
  `descripcion` varchar(150) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id_area`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mli_area`
--

LOCK TABLES `mli_area` WRITE;
/*!40000 ALTER TABLE `mli_area` DISABLE KEYS */;
INSERT INTO `mli_area` VALUES (10,'Gerencia General',1),(20,'Gerencia de Desarrollo Urbano',1),(30,'Gerencia de Administracion Tributaria',1);
/*!40000 ALTER TABLE `mli_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mli_bien`
--

DROP TABLE IF EXISTS `mli_bien`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mli_bien` (
  `id_bien` int(11) NOT NULL AUTO_INCREMENT,
  `codi_item` varchar(45) NOT NULL,
  `unidad_medida` varchar(45) DEFAULT NULL,
  `marca` varchar(45) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `categoria` varchar(45) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_bien`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mli_bien`
--

LOCK TABLES `mli_bien` WRITE;
/*!40000 ALTER TABLE `mli_bien` DISABLE KEYS */;
INSERT INTO `mli_bien` VALUES (1,'IT000011','unidad','Samsumg','Camara de Videovigilancia','1',1),(2,'IT000012','unidad','Sony','Camara Fotografica Digital','1',1),(3,'IT000013','unidad','Hewlett packard','Capturador de Imagen - Scanner','1',1),(4,'IT000014','unidad','Hewlett packard','Computadora Personal Portatil','1',1),(5,'IT000015','unidad','Motorola','Equipo de Radio Movil','1',1),(6,'IT000016','unidad','Ikasa','Escritorio de PC','2',1),(7,'IT000017','unidad','Alki','Silla Giratoria','2',1),(8,'IT000018','unidad','Arlex','Armario','2',1),(9,'IT000019','unidad','Carpyen','Mesa de Oficina','2',1),(10,'IT000020','unidad','Ikasa','Archivador','2',1),(11,'IT000021','unidad','Carpyen','Cajonera de 120x200','2',1),(12,'IT000022','unidad','Carpyen','Cajoneras Móviles','2',1),(13,'IT000023','unidad','Ikasa','Credenza','2',1),(14,'IT000024','unidad','Arlex','Mesa Ejecutiva con Puerta de Vidrio','2',1),(15,'IT000025','unidad','Arlex','Ejecutiva Mixta','2',1),(16,'IT000026','unidad','Ikasa','Estante Mixto','2',1),(17,'IT000027','unidad','Arlex','Estante con Puertas Corredizas','2',1),(18,'IT000028','unidad','Alki','Estante con Puertas','2',1),(19,'IT000029','unidad','Carpyen','Cajonera de Vidrio','2',1),(20,'IT000030','unidad','Ikasa','Credenza','2',1);
/*!40000 ALTER TABLE `mli_bien` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mli_cotizacion`
--

DROP TABLE IF EXISTS `mli_cotizacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mli_cotizacion` (
  `id_cotizacion` int(11) NOT NULL AUTO_INCREMENT,
  `id_proveedor` int(11) NOT NULL,
  `id_pedido` int(11) NOT NULL,
  `num_cotizacion` varchar(45) NOT NULL,
  `fecha_emision` date NOT NULL,
  `vigencia_hasta` date NOT NULL,
  `observaciones` text,
  `fecha_entrega_estimada` date DEFAULT NULL,
  `forma_pago` varchar(2) NOT NULL,
  `dias_pago` int(11) NOT NULL,
  `moneda` varchar(1) NOT NULL,
  `tiempo_ejecucion` int(11) DEFAULT NULL,
  `tiempo_garantia` int(11) DEFAULT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_cotizacion`),
  KEY `fk_cotizacion_proveedor_idx` (`id_proveedor`),
  KEY `fk_cotizacion_pedido_idx` (`id_pedido`),
  CONSTRAINT `fk_cotizacion_pedido` FOREIGN KEY (`id_pedido`) REFERENCES `mli_pedido` (`id_pedido`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cotizacion_proveedor` FOREIGN KEY (`id_proveedor`) REFERENCES `mli_proveedor` (`id_proveedor`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mli_cotizacion`
--

LOCK TABLES `mli_cotizacion` WRITE;
/*!40000 ALTER TABLE `mli_cotizacion` DISABLE KEYS */;
INSERT INTO `mli_cotizacion` VALUES (1,1,1,'COT00001','2017-09-28','2017-10-05','No Hay Observaciones','2017-10-15','1',2,'S',2,30,'2017-09-28 21:43:22'),(2,2,1,'COT00002','2017-09-28','2017-10-05','No Hay Observaciones','2017-10-15','1',2,'S',2,30,'2017-09-28 21:43:22'),(3,3,1,'COT00003','2017-09-28','2017-10-05','No Hay Observaciones','2017-10-15','1',2,'S',2,30,'2017-09-28 21:43:22');
/*!40000 ALTER TABLE `mli_cotizacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mli_estado`
--

DROP TABLE IF EXISTS `mli_estado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mli_estado` (
  `id_estado` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  `activo` int(1) NOT NULL,
  PRIMARY KEY (`id_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mli_estado`
--

LOCK TABLES `mli_estado` WRITE;
/*!40000 ALTER TABLE `mli_estado` DISABLE KEYS */;
INSERT INTO `mli_estado` VALUES (1,'En Proceso',1),(2,'Cotizado',1);
/*!40000 ALTER TABLE `mli_estado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mli_guia_remision`
--

DROP TABLE IF EXISTS `mli_guia_remision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mli_guia_remision` (
  `id_guia_remision` int(11) NOT NULL AUTO_INCREMENT,
  `id_orden_cs` int(11) NOT NULL,
  `numero_documento` varchar(45) NOT NULL,
  `fecha_documento` date NOT NULL,
  `observaciones` text,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_guia_remision`),
  KEY `fk_guia_remision_orden_cs_idx` (`id_orden_cs`),
  CONSTRAINT `fk_guia_remision_orden_cs` FOREIGN KEY (`id_orden_cs`) REFERENCES `mli_orden_cs` (`id_orden_cs`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mli_guia_remision`
--

DROP TABLE IF EXISTS `mli_guia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mli_guia` (
  `id_guia_remision` int(11) NOT NULL AUTO_INCREMENT,
  `numero_orden` varchar(45) NOT NULL,
  `numero_documento` varchar(45) NOT NULL,
  `solicitante` varchar(45) NOT NULL,
 `id_area` int(11) NOT NULL,
  `cantidad` varchar(45) NOT NULL,
  `almacen` varchar(45) NOT NULL,
  `ubicacion`  varchar(45) NOT NULL,
   `id_bien` int(11) NOT NULL,
   `fecha_documento` date NOT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_guia_remision`),
   KEY `fk_guia_bien` (`id_bien`),
  KEY `fk_guia_area` (`id_area`),
  CONSTRAINT `fk_guia_bien` FOREIGN KEY (`id_bien`) REFERENCES `mli_bien` (`id_bien`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_guia_area` FOREIGN KEY (`id_area`) REFERENCES `mli_area` (`id_area`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

INSERT INTO `mli_guia` (`id_guia_remision`,`numero_orden`,`numero_documento`,`solicitante`,`id_area`,`cantidad`,`almacen`,`ubicacion`,`id_bien`,`fecha_documento`,`fecha_registro`) VALUES 
 (1,'1233556','24242255','juan perez',10,'12','Almacen 1','Sistema',1,'2018-01-01','2018-04-18 00:16:55'),
 (2,'1233553','272727','salazar rios',20,'129','Almacen 2','Contabilidad',2,'2018-01-01','2018-04-18 00:16:55');

/*!40000 ALTER TABLE `mli_guia` ENABLE KEYS */;



DROP TABLE IF EXISTS `mli_informe_detalle`;
DROP TABLE IF EXISTS `mli_informe`;

                  /*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mli_informe` (
  `id_informe` int(11) NOT NULL AUTO_INCREMENT,
  `numero_solicitud` varchar(45) NOT NULL,
  `numero_informe` varchar(45) NOT NULL,
  `id_bien` int(11) NOT NULL,
  `costo` varchar(45) NOT NULL,
  `responsable` varchar(45) NOT NULL,
   `moneda` varchar(45) NOT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_informe`),
 KEY `fk_informe_idx` (`id_bien`),
  CONSTRAINT `fk_informe_idx` FOREIGN KEY (`id_bien`) REFERENCES `mli_bien` (`id_bien`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


INSERT INTO `mli_informe` (`id_informe`,`numero_solicitud`,`numero_informe`,`id_bien`,`costo`,`responsable`,`moneda`,`fecha_registro`) VALUES 
 (1,'1234','12355',1,'12','Alberto Rios','soles','2018-04-18 00:17:43'),
 (2,'2433','66165',2,'25','Raul Salas','soles','2018-04-18 00:17:43');
/*!40000 ALTER TABLE `mli_informe` ENABLE KEYS */;

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mli_informe_detalle` (
  `id_informe_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `id_informe` int(11) NOT NULL,
  `observacion` varchar(45) NOT NULL,
   PRIMARY KEY (`id_informe_detalle`),
  KEY `fk_informe_detalle_idx` (`id_informe`),
  CONSTRAINT `fk_informe_detalle_idx` FOREIGN KEY (`id_informe`) REFERENCES `mli_informe` (`id_informe`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

INSERT INTO `mli_informe_detalle` (`id_informe_detalle`,`id_informe`,`observacion`) VALUES 
 (1,1,'observacion 1.2'),
 (2,2,'observacion 2.2'),
 (3,2,'observacion 2.1'),
 (4,1,'observacion 1.2');
--
-- Dumping data for table `mli_guia_remision`
--

LOCK TABLES `mli_guia_remision` WRITE;
/*!40000 ALTER TABLE `mli_guia_remision` DISABLE KEYS */;
/*!40000 ALTER TABLE `mli_guia_remision` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mli_guia_remision_detalle`
--

DROP TABLE IF EXISTS `mli_guia_remision_detalle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mli_guia_remision_detalle` (
  `id_guia_remision_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `id_orden_cs_detalle` int(11) NOT NULL,
  `cantidad` double NOT NULL,
  PRIMARY KEY (`id_guia_remision_detalle`),
  KEY `fk_gr_orden_cs_detalle_idx` (`id_orden_cs_detalle`),
  CONSTRAINT `fk_gr_orden_cs_detalle` FOREIGN KEY (`id_orden_cs_detalle`) REFERENCES `mli_orden_cd_detalle` (`id_orden_cs_detalle`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mli_guia_remision_detalle`
--

LOCK TABLES `mli_guia_remision_detalle` WRITE;
/*!40000 ALTER TABLE `mli_guia_remision_detalle` DISABLE KEYS */;
/*!40000 ALTER TABLE `mli_guia_remision_detalle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mli_orden_cd_detalle`
--

DROP TABLE IF EXISTS `mli_orden_cd_detalle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mli_orden_cd_detalle` (
  `id_orden_cs_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `id_orden_cs` int(11) NOT NULL,
  `id_cotizacion_detalle` int(11) NOT NULL,
  `cantidad` double NOT NULL,
  PRIMARY KEY (`id_orden_cs_detalle`),
  KEY `fk_orden_cd_cotizacion_detalle_idx` (`id_cotizacion_detalle`),
  KEY `fk_orden_cs_detalle_idx` (`id_orden_cs`),
  CONSTRAINT `fk_orden_cd_cotizacion_detalle` FOREIGN KEY (`id_cotizacion_detalle`) REFERENCES `ml_cotizacion_detalle` (`id_cotizacion_detalle`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_orden_cs_detalle` FOREIGN KEY (`id_orden_cs`) REFERENCES `mli_orden_cs` (`id_orden_cs`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mli_orden_cd_detalle`
--

LOCK TABLES `mli_orden_cd_detalle` WRITE;
/*!40000 ALTER TABLE `mli_orden_cd_detalle` DISABLE KEYS */;
/*!40000 ALTER TABLE `mli_orden_cd_detalle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mli_orden_cs`
--

DROP TABLE IF EXISTS `mli_orden_cs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mli_orden_cs` (
  `id_orden_cs` int(11) NOT NULL AUTO_INCREMENT,
  `id_pedido` int(11) NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `fecha_entrega` date NOT NULL,
  `fecha_vigencia` date NOT NULL,
  `forma_pago` char(2) NOT NULL,
  `dias_plazo` int(11) NOT NULL,
  `moneda` char(1) NOT NULL,
  `tiempo_ejecucion` int(11) NOT NULL,
  `tiempo_garantia` int(11) NOT NULL,
  `observaciones` text,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_orden_cs`),
  KEY `fk_orden_pedido_idx` (`id_pedido`),
  KEY `fk_orden_proveedor_idx` (`id_proveedor`),
  CONSTRAINT `fk_orden_pedido` FOREIGN KEY (`id_pedido`) REFERENCES `mli_pedido` (`id_pedido`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_orden_proveedor` FOREIGN KEY (`id_proveedor`) REFERENCES `mli_proveedor` (`id_proveedor`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mli_orden_cs`
--

LOCK TABLES `mli_orden_cs` WRITE;
/*!40000 ALTER TABLE `mli_orden_cs` DISABLE KEYS */;
/*!40000 ALTER TABLE `mli_orden_cs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mli_padron_bienes`
--

DROP TABLE IF EXISTS `mli_padron_bienes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mli_padron_bienes` (
  `id` int(11) NOT NULL,
  `id_bien` int(11) DEFAULT NULL,
  `id_area` int(11) DEFAULT NULL,
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `id_area_idx` (`id_area`),
  KEY `id_bien` (`id_bien`),
  CONSTRAINT `id_area` FOREIGN KEY (`id_area`) REFERENCES `mli_area` (`id_area`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `id_bien` FOREIGN KEY (`id_bien`) REFERENCES `mli_bien` (`id_bien`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mli_padron_bienes`
--

LOCK TABLES `mli_padron_bienes` WRITE;
/*!40000 ALTER TABLE `mli_padron_bienes` DISABLE KEYS */;
INSERT INTO `mli_padron_bienes` VALUES (1,6,10),(2,7,10),(3,8,10),(4,9,10),(5,10,10),(6,11,10),(7,12,10),(8,13,10),(9,14,20),(10,15,20),(11,16,20),(12,17,20),(13,18,30),(14,19,30),(15,20,30);
/*!40000 ALTER TABLE `mli_padron_bienes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mli_pedido`
--

DROP TABLE IF EXISTS `mli_pedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mli_pedido` (
  `id_pedido` int(11) NOT NULL AUTO_INCREMENT,
  `cod_pedido` varchar(50) NOT NULL,
  `tipo` char(1) NOT NULL,
  `necesidad` varchar(100) NOT NULL,
  `area_pedido` int(4) NOT NULL,
  `usuario_pedido` int(4) NOT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_entrega` date NOT NULL,
  `estado` int(2) NOT NULL,
  PRIMARY KEY (`id_pedido`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mli_pedido`
--

LOCK TABLES `mli_pedido` WRITE;
/*!40000 ALTER TABLE `mli_pedido` DISABLE KEYS */;
INSERT INTO `mli_pedido` VALUES (1,'PE01','B','Mantenimiento de oficinas',10,8,'2017-09-28 21:43:22','2017-09-28',2),(2,'PE02','B','Documentación contable',10,8,'2017-09-29 00:31:40','2017-09-30',2),(3,'PE03','S','Implementación de data center',20,8,'2017-09-29 00:31:40','2017-10-10',2),(4,'PE04','B','Prueba de concepto',20,8,'2017-09-29 00:42:52','2017-11-10',2),(5,'PE05','B','Necesidad de pedido',20,4,'2017-10-10 20:45:04','2017-10-18',2),(6,'PE06','B','Solicitud de compra',10,4,'2017-10-11 01:15:34','2017-10-31',2),(7,'PE07','B','Compra de equipos de computo',20,4,'2017-10-11 02:03:38','2017-10-10',2),(8,'PE08','S','Prueba',20,4,'2018-03-24 01:10:43','2018-03-30',2);
/*!40000 ALTER TABLE `mli_pedido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mli_pedido_detalle`
--

DROP TABLE IF EXISTS `mli_pedido_detalle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mli_pedido_detalle` (
  `id_detallepedido` int(11) NOT NULL AUTO_INCREMENT,
  `id_pedido` int(11) NOT NULL,
  `cantidad` double NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `id_bien` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_detallepedido`),
  KEY `fk_pedido_detalle_idx` (`id_pedido`),
  CONSTRAINT `fk_pedido_detalle` FOREIGN KEY (`id_pedido`) REFERENCES `mli_pedido` (`id_pedido`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mli_pedido_detalle`
--

LOCK TABLES `mli_pedido_detalle` WRITE;
/*!40000 ALTER TABLE `mli_pedido_detalle` DISABLE KEYS */;
INSERT INTO `mli_pedido_detalle` VALUES (1,1,10,'Solicitud Item 1',1),(2,1,10,'Solicitud Item 2',2),(3,1,10,'Solicitud Item 3',3),(4,6,1.12,'Arroz',NULL),(5,7,12,'Ejemplo de bien',NULL),(6,7,5,'Hola',NULL),(7,8,1,'Lapicero',NULL),(8,8,3,'Ejempl',NULL);
/*!40000 ALTER TABLE `mli_pedido_detalle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mli_proveedor`
--

DROP TABLE IF EXISTS `mli_proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mli_proveedor` (
  `id_proveedor` int(11) NOT NULL AUTO_INCREMENT,
  `ruc` varchar(11) NOT NULL,
  `razon_social` varchar(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` varchar(45) NOT NULL,
  `contacto` varchar(45) NOT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_proveedor`),
  UNIQUE KEY `ruc_UNIQUE` (`ruc`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mli_proveedor`
--

LOCK TABLES `mli_proveedor` WRITE;
/*!40000 ALTER TABLE `mli_proveedor` DISABLE KEYS */;
INSERT INTO `mli_proveedor` VALUES (1,'20102938902','Proveedor de ejemplo','Lima','444-0000','Roger Arroyo','2017-10-10 19:50:24'),(2,'10102938948','Importador de prueba','Lima','212-2093','Rodrigo Abanto','2017-10-10 19:50:24'),(3,'20512893849','Fabricante local','Trujillo','512-3092','Oscar Ruiz','2017-10-10 19:50:24');
/*!40000 ALTER TABLE `mli_proveedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mun_establecimiento`
--

DROP TABLE IF EXISTS `mun_establecimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mun_establecimiento` (
  `IN_IDESTABLECIMIENTO` int(11) NOT NULL AUTO_INCREMENT,
  `VC_TRADENAME` varchar(100) NOT NULL,
  `VC_ADDRESS` varchar(100) NOT NULL,
  `DE_SCHEDULE` int(11) NOT NULL,
  `UNTIL_SCHEDULE` int(11) NOT NULL,
  `VC_ACTIVITY` varchar(50) NOT NULL,
  `IN_AREA` int(11) NOT NULL,
  `IN_IDSOLICITANTEE` int(11) NOT NULL,
  PRIMARY KEY (`IN_IDESTABLECIMIENTO`),
  KEY `IN_IDSOLICITANTEE` (`IN_IDSOLICITANTEE`),
  CONSTRAINT `mun_establecimiento_ibfk_1` FOREIGN KEY (`IN_IDSOLICITANTEE`) REFERENCES `mun_solicitante` (`IN_IDSOLICITANTE`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mun_establecimiento`
--

LOCK TABLES `mun_establecimiento` WRITE;
/*!40000 ALTER TABLE `mun_establecimiento` DISABLE KEYS */;
INSERT INTO `mun_establecimiento` VALUES (1,'LA CASA DE PEPITA','LIMA',7,16,'Venta de Comida Rapida',200,1),(2,'LA TIENDA DE DON PEPE','SAN BORJA SUR, CERCA AL RIO',7,16,'Venta de ABARROTES',200,2),(3,'SI ENTRAS NO SALES','SAN BORJA NORTE',7,16,'BAR',200,2),(4,'EL ENCANTO AMAZONICO','SAN BORJA SUR',7,16,'RESTAURANTE',200,1),(5,'LA LUCHA','SAN BORJA SUR',7,16,'Venta de Comida Rapida',200,1);
/*!40000 ALTER TABLE `mun_establecimiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mun_requisito`
--

DROP TABLE IF EXISTS `mun_requisito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mun_requisito` (
  `IN_IDREQUISITO` int(11) NOT NULL AUTO_INCREMENT,
  `VC_DESCRIPCION` varchar(100) NOT NULL,
  `DT_REGISTRY` date NOT NULL,
  `IN_IDTIPOLICENCIAR` int(11) NOT NULL,
  PRIMARY KEY (`IN_IDREQUISITO`),
  KEY `IN_IDTIPOLICENCIAR` (`IN_IDTIPOLICENCIAR`),
  CONSTRAINT `mun_requisito_ibfk_1` FOREIGN KEY (`IN_IDTIPOLICENCIAR`) REFERENCES `mun_tiposolicitud` (`IN_IDTIPOLICENCIA`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mun_requisito`
--

LOCK TABLES `mun_requisito` WRITE;
/*!40000 ALTER TABLE `mun_requisito` DISABLE KEYS */;
INSERT INTO `mun_requisito` VALUES (1,'Planos a la vista del inspector','2017-09-13',1),(2,'Letreros de señalizacion','2017-09-13',1),(3,'Copia simple del sector correspondiente','2017-09-13',1),(4,'Copia simple si la actividad economica es relacionado a la salud','2017-09-13',1),(5,'Buenos materiales','2017-09-13',2),(6,'Presentar autorizacion del ministerio de transporte y minas','2017-09-13',2);
/*!40000 ALTER TABLE `mun_requisito` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mun_solicitante`
--

DROP TABLE IF EXISTS `mun_solicitante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mun_solicitante` (
  `IN_IDSOLICITANTE` int(11) NOT NULL AUTO_INCREMENT,
  `VC_NAME` varchar(100) NOT NULL,
  `VC_LASTNAME` varchar(100) NOT NULL,
  `VC_LASTNAME1` varchar(100) NOT NULL,
  `VC_MAIL` varchar(50) NOT NULL,
  `VC_PHONE` varchar(20) NOT NULL,
  `VC_NUMBERDOC` varchar(15) NOT NULL,
  `VC_ADDRESS` varchar(50) NOT NULL,
  `DT_REGISTRY` date NOT NULL,
  `IN_IDTIPODOCUMENTO` int(11) NOT NULL,
  `IN_IDTIPOPERSONA` int(11) NOT NULL,
  PRIMARY KEY (`IN_IDSOLICITANTE`),
  KEY `IN_IDTIPODOCUMENTO` (`IN_IDTIPODOCUMENTO`),
  KEY `IN_IDTIPOPERSONA` (`IN_IDTIPOPERSONA`),
  CONSTRAINT `mun_solicitante_ibfk_1` FOREIGN KEY (`IN_IDTIPODOCUMENTO`) REFERENCES `mun_tipodocumento` (`IN_IDTIPODOCUMENTO`),
  CONSTRAINT `mun_solicitante_ibfk_2` FOREIGN KEY (`IN_IDTIPOPERSONA`) REFERENCES `mun_tipopersona` (`IN_IDTIPOPERSONA`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mun_solicitante`
--

LOCK TABLES `mun_solicitante` WRITE;
/*!40000 ALTER TABLE `mun_solicitante` DISABLE KEYS */;
INSERT INTO `mun_solicitante` VALUES (1,'JOSE IGNACIO','LOPEZ','VALENCIA','joselv772@gmail.com','943946637','46265898','LIMA','2017-09-13',1,1),(2,'JESUS RONAL','GOMEZ','HERRERA','jesu.ronal@gmail.com','959595632','10109311','LIMA','2017-09-13',1,1);
/*!40000 ALTER TABLE `mun_solicitante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mun_solicitud`
--

DROP TABLE IF EXISTS `mun_solicitud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mun_solicitud` (
  `IN_IDSOLICITUD` int(11) NOT NULL AUTO_INCREMENT,
  `DT_REGISTRY` date NOT NULL,
  `DT_UPDATE` date NOT NULL,
  `CH_STATE` varchar(1) NOT NULL,
  `IN_IDTIPOLICENCIAS` int(11) NOT NULL,
  `IN_IDSOLICITANTE` int(11) NOT NULL,
  `IN_IDESTABLECIMIENTO` int(11) NOT NULL,
  `IN_IDREQUISITO` int(11) NOT NULL,
  PRIMARY KEY (`IN_IDSOLICITUD`),
  KEY `IN_IDTIPOLICENCIAS` (`IN_IDTIPOLICENCIAS`),
  KEY `IN_IDSOLICITANTE` (`IN_IDSOLICITANTE`),
  KEY `IN_IDESTABLECIMIENTO` (`IN_IDESTABLECIMIENTO`),
  KEY `IN_IDREQUISITO` (`IN_IDREQUISITO`),
  CONSTRAINT `mun_solicitud_ibfk_1` FOREIGN KEY (`IN_IDTIPOLICENCIAS`) REFERENCES `mun_tiposolicitud` (`IN_IDTIPOLICENCIA`),
  CONSTRAINT `mun_solicitud_ibfk_2` FOREIGN KEY (`IN_IDSOLICITANTE`) REFERENCES `mun_solicitante` (`IN_IDSOLICITANTE`),
  CONSTRAINT `mun_solicitud_ibfk_3` FOREIGN KEY (`IN_IDESTABLECIMIENTO`) REFERENCES `mun_establecimiento` (`IN_IDESTABLECIMIENTO`),
  CONSTRAINT `mun_solicitud_ibfk_4` FOREIGN KEY (`IN_IDREQUISITO`) REFERENCES `mun_requisito` (`IN_IDREQUISITO`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mun_solicitud`
--

LOCK TABLES `mun_solicitud` WRITE;
/*!40000 ALTER TABLE `mun_solicitud` DISABLE KEYS */;
INSERT INTO `mun_solicitud` VALUES (1,'2017-09-13','2017-09-13','4',1,1,1,1),(2,'2017-09-13','2017-09-13','4',2,2,1,1),(3,'2017-09-13','2017-09-13','3',2,2,1,1),(4,'2017-09-13','2017-09-13','2',1,2,1,1),(5,'2017-09-13','2017-09-13','5',1,1,2,1),(6,'2017-09-13','2017-09-13','2',1,1,3,1),(7,'2017-09-13','2017-09-13','2',1,2,1,1),(8,'2017-09-13','2017-09-13','2',1,2,2,1),(9,'2017-09-13','2017-09-13','2',1,1,3,1),(10,'2017-09-13','2017-09-13','2',1,1,4,1),(11,'2017-09-13','2017-09-13','2',1,1,2,1),(12,'2017-09-13','2017-09-13','2',1,2,3,1),(13,'2017-09-13','2017-09-13','2',1,2,3,1);
/*!40000 ALTER TABLE `mun_solicitud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mun_tipodocumento`
--

DROP TABLE IF EXISTS `mun_tipodocumento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mun_tipodocumento` (
  `IN_IDTIPODOCUMENTO` int(11) NOT NULL AUTO_INCREMENT,
  `VC_NAME` varchar(30) NOT NULL,
  `DT_REGISTRY` date NOT NULL,
  PRIMARY KEY (`IN_IDTIPODOCUMENTO`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mun_tipodocumento`
--

LOCK TABLES `mun_tipodocumento` WRITE;
/*!40000 ALTER TABLE `mun_tipodocumento` DISABLE KEYS */;
INSERT INTO `mun_tipodocumento` VALUES (1,'DNI','2017-09-13'),(2,'CE','2017-09-13'),(3,'RUC','2017-09-13');
/*!40000 ALTER TABLE `mun_tipodocumento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mun_tipopersona`
--

DROP TABLE IF EXISTS `mun_tipopersona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mun_tipopersona` (
  `IN_IDTIPOPERSONA` int(11) NOT NULL AUTO_INCREMENT,
  `VC_NAME` varchar(30) NOT NULL,
  `DT_REGISTRY` date NOT NULL,
  PRIMARY KEY (`IN_IDTIPOPERSONA`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mun_tipopersona`
--

LOCK TABLES `mun_tipopersona` WRITE;
/*!40000 ALTER TABLE `mun_tipopersona` DISABLE KEYS */;
INSERT INTO `mun_tipopersona` VALUES (1,'PERSONA JURIDICA','2017-09-13'),(2,'PERSONA NATURAL','2017-09-13');
/*!40000 ALTER TABLE `mun_tipopersona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mun_tiposolicitud`
--

DROP TABLE IF EXISTS `mun_tiposolicitud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mun_tiposolicitud` (
  `IN_IDTIPOLICENCIA` int(11) NOT NULL AUTO_INCREMENT,
  `VC_NAME` varchar(50) NOT NULL,
  `VC_DETAIL` varchar(50) DEFAULT NULL,
  `DT_REGISTRY` date NOT NULL,
  PRIMARY KEY (`IN_IDTIPOLICENCIA`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mun_tiposolicitud`
--

LOCK TABLES `mun_tiposolicitud` WRITE;
/*!40000 ALTER TABLE `mun_tiposolicitud` DISABLE KEYS */;
INSERT INTO `mun_tiposolicitud` VALUES (1,'LICENCIA FUNCIONAMIENTO','LICENCIA FUNCIONAMIENTO','2017-09-13'),(2,'LICENCIA EDIFICACION','LICENCIA EDIFICACION','2017-09-13');
/*!40000 ALTER TABLE `mun_tiposolicitud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `vista_solicitudes`
--

DROP TABLE IF EXISTS `vista_solicitudes`;
/*!50001 DROP VIEW IF EXISTS `vista_solicitudes`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vista_solicitudes` AS SELECT 
 1 AS `CODIGO`,
 1 AS `NOMBRE`,
 1 AS `APELLIDO`,
 1 AS `APELLIDO1`,
 1 AS `FECHA`,
 1 AS `ESTADO`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping events for database 'mun_sanborja'
--

--
-- Dumping routines for database 'mun_sanborja'
--

--
-- Final view structure for view `vista_solicitudes`
--

/*!50001 DROP VIEW IF EXISTS `vista_solicitudes`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vista_solicitudes` AS select `s`.`IN_IDSOLICITUD` AS `CODIGO`,`so`.`VC_NAME` AS `NOMBRE`,`so`.`VC_LASTNAME` AS `APELLIDO`,`so`.`VC_LASTNAME1` AS `APELLIDO1`,`s`.`DT_REGISTRY` AS `FECHA`,`s`.`CH_STATE` AS `ESTADO` from (`mun_solicitud` `s` join `mun_solicitante` `so` on((`s`.`IN_IDSOLICITANTE` = `so`.`IN_IDSOLICITANTE`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-15 22:12:54
