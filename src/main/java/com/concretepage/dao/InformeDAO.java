package com.concretepage.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.concretepage.entity.Informe;
import com.concretepage.entity.InformeDetalle;
import com.concretepage.entity.Pedido;
import com.concretepage.entity.PedidoDetalle;

@Transactional
@Repository
public class InformeDAO implements IInformeDAO {
	@PersistenceContext	
	private EntityManager entityManager;

	@Override
	public List<Informe> getAllInforme(String solicitud,String responsable) {
	List<Informe> det = entityManager.createQuery(
		    "select cd " +
		    		"from Informe cd "+
		    		"join cd.bien ph " +
		    		"where cd.numeroSolicitud like :solicitud and cd.responsable like :responsable", Informe.class )
			.setParameter("solicitud",solicitud).setParameter("responsable",responsable)
			.getResultList();
	return  det;
	
	}
	
	@Override
	public Informe getInforme(int idInforme) {
		return entityManager.find(Informe.class, idInforme);
	
	}
	
	@Override
	public List<InformeDetalle> getAllInformeDetalle(String informe) {
		
	List<InformeDetalle> det  = null;
		
	String query_ ="select cd " +
    		"from InformeDetalle cd "+
   		 "join cd.informe ph " +
   		"join ph.bien ph1 ";
	
	if(!informe.equals("%")) {
		
		det= entityManager.createQuery(
				 query_ +" where ph.idInforme=:informeid", InformeDetalle.class )
					.setParameter("informeid",Integer.parseInt(informe))
					.getResultList();
		 
	}else {
		
		det=	 entityManager.createQuery(
				 query_ , InformeDetalle.class )
					.getResultList();
		
	}
	return  det;
	
	}
	
	
	
	@Transactional
	public int insertInformeDetalle(Informe informe,String observacion) {
		
		
try {
	
	InformeDetalle detalle = new InformeDetalle();
	detalle.setObservacion(observacion);
	detalle.setInforme(informe);
	entityManager.persist(detalle);
		return 1;
}catch (Exception e) {
	System.out.println(e.toString());
	return -1;
}
		
	}
	
	
	
	
	
	

}

