package com.concretepage.dao;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import com.concretepage.entity.Pedido;

@Transactional
@Repository

public class PedidoDAO implements IPedidoDAO {
	@PersistenceContext	
	private EntityManager entityManager;	

	@SuppressWarnings("unchecked")
	@Override
	public List<Pedido> getAllPedidos() {
		String hql = "FROM Pedido as atcl ORDER BY atcl.idPedido";
		return (List<Pedido>) entityManager.createQuery(hql).getResultList();
	}

	@Override
	public Pedido getArticleById(int idPedido) {
		return entityManager.find(Pedido.class, idPedido);
	}

	@Override
	public void addArticle(Pedido pedido) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateArticle(Pedido pedido) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteArticle(int idPedido) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean articleExists(String title, String category) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Pedido> getAllPedido(int idPedido) {
		// TODO Auto-generated method stub
		List<Pedido> det = entityManager.createQuery(
			    "select pr " +
			    "from Pedido pr " +
			    "where pr.idPedido = :idPedido", Pedido.class )
			.setParameter( "idPedido", idPedido )
			.getResultList();
		return  det;
	}

}
