package com.concretepage.dao;

import java.util.List;

import com.concretepage.entity.CotizacionDetalle;

public interface ICotizacionDetDAO {
	List<CotizacionDetalle> getAllCotizacionDet(int id);
	String updateConsolidado(String arr);
	

}
