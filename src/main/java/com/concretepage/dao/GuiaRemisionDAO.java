package com.concretepage.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.concretepage.entity.Guia;
import com.concretepage.entity.PedidoDetalle;

@Transactional
@Repository
public class GuiaRemisionDAO implements IGuiaRemisionDAO {
	@PersistenceContext	
	private EntityManager entityManager;

	@Override
	public List<Guia> getAllGuiaRemision(String item,String area,String orden , String guia,String solicitante,String tipodocumento,String fecha) {
	List<Guia> det = entityManager.createQuery(
		    "select cd " +
		    		"from Guia cd "+
		    		"join cd.bien bi " +
		    		"join cd.area ar " +
		    		" where bi.codItem like :item and ar.descripcion like :area and cd.numeroOrden like :orden and cd.numeroDocumento like :guia and cd.solicitante like :solicitante", Guia.class )
			.setParameter("item",item).setParameter("area",area).setParameter("orden",orden).setParameter("guia",guia).setParameter("solicitante",solicitante)
			.getResultList();
	return  det;
	
	}

}

