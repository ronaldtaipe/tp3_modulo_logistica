package com.concretepage.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.concretepage.entity.CotizacionDetalle;

@Transactional
@Repository
public class CotizacionDetDAO implements ICotizacionDetDAO {
	@PersistenceContext	
	private EntityManager entityManager;

	@Override
	public List<CotizacionDetalle> getAllCotizacionDet(int id) {
	List<CotizacionDetalle> det = entityManager.createQuery(
		    "select cd " +
		    		"from CotizacionDetalle cd " +
		    		"join cd.pedetalle det " +
		    		"join det.pedido pe "+
		    		"join cd.cotizacion co "+
		    		"where pe.idPedido = :idPedido", CotizacionDetalle.class )
			.setParameter("idPedido", id)
			.getResultList();
	return  det;
	}

	@Override
	public String updateConsolidado(String arr) {
		// TODO Auto-generated method stub
		String mensaje ="";
		String[] ids = arr.split(";");
		
		String query=	"update CotizacionDetalle cd " +
						"set cd.aprobada=1 "+
						"where cd.idCotizacionDetalle=:idCodDet ";
						
		for(int i=0;i<ids.length;i++) {
			int map =  entityManager.createQuery(query)
											.setParameter("idCodDet", Integer.parseInt(ids[i]))
											.executeUpdate();
		}
		
		if (ids.length > 1) {
			mensaje = "Cotización ha sido Aprobada";
		} else {
			mensaje = "Las Cotizaciones han sido Aprobadas";
		}
		
		return mensaje;
	}

}

