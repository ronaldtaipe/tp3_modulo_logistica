package com.concretepage.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.concretepage.entity.Pedido;
import com.concretepage.entity.PedidoDetalle;

@Transactional
@Repository

public class PedidoDetDAO implements IPedidoDetDAO{
	@PersistenceContext	
	private EntityManager entityManager;

	@Override
	public List<PedidoDetalle> getPedidoById(int idPedido) {
		List<PedidoDetalle> det = entityManager.createQuery(
			    "select pr " +
			    "from PedidoDetalle pr " +
			    "join pr.pedido ph " +
			    "join pr.bien bi "+
			    "where ph.idPedido = :idPedido", PedidoDetalle.class )
			.setParameter( "idPedido", idPedido )
			.getResultList();
		return  det;
	}

	@Override
	public List<PedidoDetalle> getCotizacion(int id) {
		// TODO Auto-generated method stub
		return null;
	}	

	
}
