package com.concretepage.dao;

import java.util.List;

import com.concretepage.entity.Informe;
import com.concretepage.entity.InformeDetalle;

public interface IInformeDAO {
	public List<Informe> getAllInforme(String solicitud,String responsable);

	public List<InformeDetalle> getAllInformeDetalle(String informe);
	
	
	public Informe getInforme(int idInforme) ;
	public int insertInformeDetalle(Informe informe,String observacion) ;
}
