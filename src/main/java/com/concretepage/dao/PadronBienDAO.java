package com.concretepage.dao;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.concretepage.entity.PadronBien;
import com.concretepage.entity.PedidoDetalle;
import com.concretepage.entity.listas.Grafico;
@Transactional
@Repository

public class PadronBienDAO implements IPadronBienDAO {
	@PersistenceContext	
	private EntityManager entityManager;
	@Override
	public List<PedidoDetalle> getPadronBien(String fechaEntregaIni,String fechaEntregaFin,int estado) throws Exception {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		Date dateFrom = format.parse(fechaEntregaIni);
		Date enDate = format.parse(fechaEntregaFin);
		List<PedidoDetalle> det = entityManager.createQuery("select pr from "+ 
															"PedidoDetalle pr " +
															"join pr.pedido ph "+
															"join pr.bien bi "+
															"where bi.estado=:estado and ph.fechaEntrega BETWEEN :stDate AND :edDate", PedidoDetalle.class )
															.setParameter("estado",estado)
															.setParameter("stDate",dateFrom)
															.setParameter("edDate",enDate)
															.getResultList();
		return  det;
	}
	@Override
	public List<PadronBien> getBienGrafico()throws Exception {
		// TODO Auto-generated method stub
		List<PadronBien>pbien =entityManager.createQuery("select count(pb),ar.descripcion from "+ 
				"PadronBien pb " +
				"join pb.area ar "+
				"group by ar.descripcion ", PadronBien.class )
				.getResultList();
		Map<String, String> map = new HashMap<>();
		return pbien;
	}
	@Override
	public List<Grafico> getBienGraficos() {
		@SuppressWarnings("unchecked")
		List<Object[]>map =  entityManager.createQuery("select cast(count(pb) as string),ar.descripcion from "+ 
				"PadronBien pb " +
				"join pb.area ar "+
				"group by ar.descripcion ").getResultList();
	
		List<Grafico> list= new ArrayList<Grafico>();
		Iterator it = map.iterator();
		while(it.hasNext()){
		     Object[] line = (Object[]) it.next();
		     Grafico eq = new Grafico();
		     eq.setIdConta(line[0].toString());
		     eq.setAreaDesc(line[1].toString());		     		     
		     list.add(eq);		     
		}
		System.out.println(list.size());
		
		return list;
		// return listObj.toJSONString();
	}

}
