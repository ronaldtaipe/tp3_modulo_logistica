package com.concretepage.dao;

import com.concretepage.entity.Pedido;
import java.util.List;

public interface IPedidoDAO {
	
	List<Pedido> getAllPedidos();
    
	Pedido getArticleById(int idPedido);
    
	void addArticle(Pedido pedido);
    
	void updateArticle(Pedido pedido);
    
	void deleteArticle(int idPedido);
    
	boolean articleExists(String title, String category);
	List<Pedido> getAllPedido(int idPedido);
    

}
