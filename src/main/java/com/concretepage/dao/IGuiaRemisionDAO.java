package com.concretepage.dao;

import java.util.List;

import com.concretepage.entity.Guia;

public interface IGuiaRemisionDAO {
	public List<Guia> getAllGuiaRemision(String item,String area,String orden , String guia,String solicitante,String tipodocumento,String fecha);

}
