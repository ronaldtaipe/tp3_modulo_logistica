package com.concretepage.dao;


import java.util.List;

import com.concretepage.entity.PedidoDetalle;

public interface IPedidoDetDAO {
	List<PedidoDetalle> getPedidoById(int idPedido);
	List<PedidoDetalle>getCotizacion(int id);

}
