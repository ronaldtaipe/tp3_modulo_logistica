package com.concretepage.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="ml_cotizacion_detalle")
public class CotizacionDetalle implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_cotizacion_detalle")
    private int idCotizacionDetalle;
	
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_cotizacion")
	private Cotizacion cotizacion;
	
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_pedido_detalle")
	private PedidoDetalle pedetalle;
	
	@Column(name="cantidad")
	private int cantidad;
	
	@Column(name="descripcion")
	private String descripcion;
	
	@Column(name="valor_venta")
	private double valorVenta;
	
	@Column(name="f_aprobada")
	private int aprobada;


	public int getIdCotizacionDetalle() {
		return idCotizacionDetalle;
	}

	public void setIdCotizacionDetalle(int idCotizacionDetalle) {
		this.idCotizacionDetalle = idCotizacionDetalle;
	}

	public Cotizacion getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(Cotizacion cotizacion) {
		this.cotizacion = cotizacion;
	}

	public PedidoDetalle getPedetalle() {
		return pedetalle;
	}

	public void setPedetalle(PedidoDetalle pedetalle) {
		this.pedetalle = pedetalle;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getValorVenta() {
		return valorVenta;
	}

	public void setValorVenta(double valorVenta) {
		this.valorVenta = valorVenta;
	}

	public int getAprobada() {
		return aprobada;
	}

	public void setAprobada(int aprobada) {
		this.aprobada = aprobada;
	}
	
	

}
