
package com.concretepage.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="mli_cotizacion")

public class Cotizacion implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_cotizacion")
    private int idCotizacion;
	
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_proveedor")
    private Proveedor proveedor;
	
	@Column(name="num_cotizacion")
    private String numCotizacion;	//codigo de pedido patron PE01
	
	@Column(name="fecha_emision")
	private String fechaEmision;
	
	@Column(name="vigencia_hasta")
	private String vigencia_hasta;
	
	@Column(name="fecha_entrega_estimada")
	private String fechaEntegaEstimada;
	
	@Column(name="fecha_registro")
	private String fechaRegistro;
	
	@Column(name="forma_pago")
	private String formaPago;
	
	@Column(name="dias_pago")
	private int diasPago;
	
	
	@Column(name="moneda")
	private String moneda;
	
	@Column(name="tiempo_ejecucion")
	private String tiempoEjecucion;
	
	
	@Column(name="tiempo_garantia")
	private String tiempoGarantia;
	
	@OneToMany(mappedBy = "cotizacion")
	List<CotizacionDetalle>coddet = new ArrayList<CotizacionDetalle>();
	
	public List<CotizacionDetalle> getCoddet() {
		return coddet;
	}

	public void setCoddet(List<CotizacionDetalle> coddet) {
		this.coddet = coddet;
	}


	public int getIdCotizacion() {
		return idCotizacion;
	}


	public void setIdCotizacion(int idCotizacion) {
		this.idCotizacion = idCotizacion;
	}


	public Proveedor getProveedor() {
		return proveedor;
	}


	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}



	public String getNumCotizacion() {
		return numCotizacion;
	}

	public void setNumCotizacion(String numCotizacion) {
		this.numCotizacion = numCotizacion;
	}

	public String getFechaEmision() {
		return fechaEmision;
	}


	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}


	public String getVigencia_hasta() {
		return vigencia_hasta;
	}


	public void setVigencia_hasta(String vigencia_hasta) {
		this.vigencia_hasta = vigencia_hasta;
	}


	public String getFechaEntegaEstimada() {
		return fechaEntegaEstimada;
	}


	public void setFechaEntegaEstimada(String fechaEntegaEstimada) {
		this.fechaEntegaEstimada = fechaEntegaEstimada;
	}


	public String getFechaRegistro() {
		return fechaRegistro;
	}


	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}


	public String getFormaPago() {
		return formaPago;
	}


	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}


	public int getDiasPago() {
		return diasPago;
	}


	public void setDiasPago(int diasPago) {
		this.diasPago = diasPago;
	}


	public String getMoneda() {
		return moneda;
	}


	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}


	public String getTiempoEjecucion() {
		return tiempoEjecucion;
	}


	public void setTiempoEjecucion(String tiempoEjecucion) {
		this.tiempoEjecucion = tiempoEjecucion;
	}


	public String getTiempoGarantia() {
		return tiempoGarantia;
	}


	public void setTiempoGarantia(String tiempoGarantia) {
		this.tiempoGarantia = tiempoGarantia;
	}
		

}

