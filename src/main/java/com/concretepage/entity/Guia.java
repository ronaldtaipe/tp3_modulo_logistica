
package com.concretepage.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="mli_guia")

public class Guia implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_guia_remision")
    private int idGuia;
	
	@Column(name="numero_orden")
    private String numeroOrden;	//codigo de pedido patron PE01
	
	@Column(name="numero_documento")
    private String numeroDocumento;	//codigo de pedido patron PE01
	
	@Column(name="solicitante")
    private String solicitante;	//codigo de pedido patron PE01
	
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_area")
	private Area area;
	
	@Column(name="cantidad")
    private String cantidad;	//codigo de pedido patron PE01
	
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_bien")
	private Bien bien;
	
	
	@Column(name="almacen")
    private String almacen;	//codigo de pedido patron PE01
	
	@Column(name="ubicacion")
    private String ubicacion;	//codigo de pedido patron PE01
	
	
		
	
	@Column(name="fecha_documento")
	private String fechaDocumento;
	
	
	@Column(name="fecha_registro")
	private String fechaRegistro;


	public int getIdGuia() {
		return idGuia;
	}


	public void setIdGuia(int idGuia) {
		this.idGuia = idGuia;
	}


	public String getNumeroOrden() {
		return numeroOrden;
	}


	public void setNumeroOrden(String numeroOrden) {
		this.numeroOrden = numeroOrden;
	}


	public String getNumeroDocumento() {
		return numeroDocumento;
	}


	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}


	public String getSolicitante() {
		return solicitante;
	}


	public void setSolicitante(String solicitante) {
		this.solicitante = solicitante;
	}


	


	public String getCantidad() {
		return cantidad;
	}


	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}




	public String getAlmacen() {
		return almacen;
	}


	public void setAlmacen(String almacen) {
		this.almacen = almacen;
	}


	public String getUbicacion() {
		return ubicacion;
	}


	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}


	

	public String getFechaDocumento() {
		return fechaDocumento;
	}


	public void setFechaDocumento(String fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}


	public String getFechaRegistro() {
		return fechaRegistro;
	}


	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public Area getArea() {
		return area;
	}


	public void setArea(Area area) {
		this.area = area;
	}


	public Bien getBien() {
		return bien;
	}


	public void setBien(Bien bien) {
		this.bien = bien;
	}

	

}

