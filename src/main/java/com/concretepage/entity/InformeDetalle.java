package com.concretepage.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="mli_informe_detalle")
public class InformeDetalle implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_informe_detalle")
    private int idInformeDetalle;
	
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_informe")
	private Informe informe;
	
	
	@Column(name="observacion")
	private String observacion;


	public int getIdInformeDetalle() {
		return idInformeDetalle;
	}


	public void setIdInformeDetalle(int idInformeDetalle) {
		this.idInformeDetalle = idInformeDetalle;
	}


	public Informe getInforme() {
		return informe;
	}


	public void setInforme(Informe informe) {
		this.informe = informe;
	}


	public String getObservacion() {
		return observacion;
	}


	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
