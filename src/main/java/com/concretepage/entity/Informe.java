
package com.concretepage.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="mli_informe")


public class Informe implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_informe")
    private int idInforme;
	
	@Column(name="numero_solicitud")
    private String numeroSolicitud;	//codigo de pedido patron PE01
	
	@Column(name="numero_informe")
    private String numeroInforme;	//codigo de pedido patron PE01
	
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_bien")
	private Bien bien;
	
	@Column(name="costo")
    private String costo;	//codigo de pedido patron PE01

	@Column(name="moneda")
    private String moneda;	//codigo de pedido patron PE01
	
	
	@Column(name="responsable")
    private String responsable;	//codigo de pedido patron PE01
	@Column(name="fecha_registro")
	private String fechaRegistro;

	@OneToMany(mappedBy = "informe")
    private List<InformeDetalle> infdet = new ArrayList<InformeDetalle>();

	public int getIdInforme() {
		return idInforme;
	}

	public void setIdInforme(int idInforme) {
		this.idInforme = idInforme;
	}

	public String getNumeroSolicitud() {
		return numeroSolicitud;
	}

	public void setNumeroSolicitud(String numeroSolicitud) {
		this.numeroSolicitud = numeroSolicitud;
	}

	public String getNumeroInforme() {
		return numeroInforme;
	}

	public void setNumeroInforme(String numeroInforme) {
		this.numeroInforme = numeroInforme;
	}

	public Bien getBien() {
		return bien;
	}

	public void setBien(Bien bien) {
		this.bien = bien;
	}

	public String getCosto() {
		return costo;
	}

	public void setCosto(String costo) {
		this.costo = costo;
	}

	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	public String getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public List<InformeDetalle> getInfdet() {
		return infdet;
	}

	public void setInfdet(List<InformeDetalle> infdet) {
		this.infdet = infdet;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	
	
	

}

