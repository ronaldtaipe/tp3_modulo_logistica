
package com.concretepage.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="mli_proveedor")

public class Proveedor implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_proveedor")
    private int idProveedor;
	
	@Column(name="ruc")
    private String ruc;	//codigo de pedido patron PE01
	
	@Column(name="direccion")
	private String direccion;
	
	@Column(name="razon_social")
	private String razonSocial;

	@Column(name="telefono")
	private String telefono;
	
	@Column(name="contacto")
	private String contacto;
	
	@Column(name="fecha_registro")
	private String fechaRegistro;
	
	@OneToMany(mappedBy = "proveedor")
	private List<Cotizacion>cotizacion = new ArrayList<Cotizacion>();

	public List<Cotizacion> getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(List<Cotizacion> cotizacion) {
		this.cotizacion = cotizacion;
	}

	public int getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}


	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	
	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public String getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	
	
	
	

}

