package com.concretepage.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="mli_bien")

public class Bien implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_bien")
    private int idBien;
	
	@Column(name="codi_item")
    private String codItem;	//codigo de pedido patron PE01
	
	@Column(name="unidad_medida")
	private String unidadMedida;
	
	@Column(name="marca")
	private String marca;
	
	@Column(name="descripcion")
	private String descripcion;
	
	@Column(name="categoria")
	private int categoria;
	
	@Column(name="estado")
	private int estado;

	@OneToMany(cascade = CascadeType.ALL,mappedBy="bien")
	List<PedidoDetalle>peddet=new ArrayList<PedidoDetalle>();
	
	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public List<PedidoDetalle> getPeddet() {
		return peddet;
	}

	public void setPeddet(List<PedidoDetalle> peddet) {
		this.peddet = peddet;
	}

	public int getIdBien() {
		return idBien;
	}

	public void setIdBien(int idBien) {
		this.idBien = idBien;
	}

	public String getCodItem() {
		return codItem;
	}

	public void setCodItem(String codItem) {
		this.codItem = codItem;
	}

	public String getUnidadMedida() {
		return unidadMedida;
	}

	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getCategoria() {
		return categoria;
	}

	public void setCategoria(int categoria) {
		this.categoria = categoria;
	}
	
	
	
	

}
