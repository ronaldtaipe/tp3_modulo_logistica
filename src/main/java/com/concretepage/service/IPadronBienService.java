package com.concretepage.service;


import java.util.List;


import com.concretepage.entity.PadronBien;
import com.concretepage.entity.PedidoDetalle;
import com.concretepage.entity.listas.Grafico;

public interface IPadronBienService {
	List<PedidoDetalle> getPedidoById(String fechaEntregaIni,String fechaEntregaFin,int estado) throws Exception;	
	List<PadronBien>getBienGrafico() throws Exception;
	List<Grafico> getBienGraficos();
}
