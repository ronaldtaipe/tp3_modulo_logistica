package com.concretepage.service;

import java.util.List;
import com.concretepage.entity.Pedido;

public interface IPedidoService {
	
	List<Pedido> getAllPedidos();
	List<Pedido> getAllPedido(int idPedido);
	
}
