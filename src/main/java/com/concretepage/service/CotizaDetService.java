package com.concretepage.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.concretepage.dao.CotizacionDetDAO;
import com.concretepage.entity.CotizacionDetalle;

@Service
public class CotizaDetService implements ICotizaDetService{
	
	@Autowired
	CotizacionDetDAO cotdet;
	@Override
	public List<CotizacionDetalle> getCotizacion(int idPedido) {
		// TODO Auto-generated method stub`´ñ` 
		return cotdet.getAllCotizacionDet(idPedido);
	}
	@Override
	public String updateConsolidado(String arr) {
		// TODO Auto-generated method stub
		return cotdet.updateConsolidado(arr);
	}

}
