package com.concretepage.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.concretepage.dao.InformeDAO;
import com.concretepage.entity.Informe;
import com.concretepage.entity.InformeDetalle;

@Service	
public class InformeService implements IInformeService{
	
	@Autowired
	InformeDAO cotdet;
	@Override
	public List<Informe> getAllInforme(String solicitud,String responsable) {
		// TODO Auto-generated method stub`´ñ` 
		return cotdet.getAllInforme(solicitud,responsable);
	}

	@Override
	public List<InformeDetalle> getAllInformeDetalle(String informe) {
		// TODO Auto-generated method stub`´ñ` 
		return cotdet.getAllInformeDetalle(informe);
	}
	@Override
	public Informe getInforme(int idInforme) {
		return cotdet.getInforme(idInforme);
	}
	
	@Override
	public int insertInformeDetalle(Informe informe,String observacion) {
		// TODO Auto-generated method stub`´ñ` 
		return cotdet.insertInformeDetalle(informe,observacion);
	}
	

}

