package com.concretepage.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.concretepage.dao.IPedidoDAO;
import com.concretepage.entity.Pedido;

@Service
public class PedidoService implements IPedidoService {
	@Autowired
	private IPedidoDAO ipedidodao;
	
	@Override
	public List<Pedido> getAllPedidos() {
		// TODO Auto-generated method stub
		return ipedidodao.getAllPedidos();
	}

	@Override
	public List<Pedido> getAllPedido(int idPedido) {
		// TODO Auto-generated method stub
		return ipedidodao.getAllPedido(idPedido);
	}

}
