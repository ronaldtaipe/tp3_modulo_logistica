package com.concretepage.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.concretepage.dao.IPedidoDetDAO;
import com.concretepage.entity.PedidoDetalle;

@Service
public class PedidoDetService implements IPedidoDetService{
	@Autowired
	IPedidoDetDAO detdao;
	@Override
	public List<PedidoDetalle> getPedidoById(int idPedido) {
		// TODO Auto-generated method stub
		return detdao.getPedidoById(idPedido);
	}

}
