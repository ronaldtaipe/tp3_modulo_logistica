package com.concretepage.service;

import java.util.List;

import com.concretepage.entity.Guia;

public interface IGuiaRemisionService {
	List<Guia> getAllGuiaRemision(String item,String area,String orden , String guia,String solicitante,String tipodocumento,String fecha);
}
