package com.concretepage.service;

import java.util.List;

import com.concretepage.entity.Informe;
import com.concretepage.entity.InformeDetalle;

public interface IInformeService {
	List<Informe> getAllInforme(String solicitud,String responsable);
	List<InformeDetalle> getAllInformeDetalle(String informe);
	
	public Informe getInforme(int idInforme) ;
	int insertInformeDetalle(Informe informe,String observacion);
}
