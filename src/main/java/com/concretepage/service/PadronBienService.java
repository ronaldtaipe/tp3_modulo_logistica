package com.concretepage.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.concretepage.dao.IPadronBienDAO;
import com.concretepage.entity.PadronBien;
import com.concretepage.entity.PedidoDetalle;
import com.concretepage.entity.listas.Grafico;
@Service
public class PadronBienService implements IPadronBienService{
	@Autowired
	IPadronBienDAO paddao;
	@Override
	public List<PedidoDetalle> getPedidoById(String fechaEntregaIni, String fechaEntregaFin,int estado) throws Exception {
		// TODO Auto-generated method stub
		return paddao.getPadronBien(fechaEntregaIni, fechaEntregaFin,estado);
	}
	@Override
	public List<PadronBien> getBienGrafico() throws Exception {
		// TODO Auto-generated method stub
		return paddao.getBienGrafico();
	}
	@Override
	public List<Grafico> getBienGraficos() {
		// TODO Auto-generated method stub
		return paddao.getBienGraficos();
	}

}
