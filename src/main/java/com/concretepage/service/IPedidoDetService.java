package com.concretepage.service;

import java.util.List;

import com.concretepage.entity.PedidoDetalle;

public interface IPedidoDetService {
	List<PedidoDetalle> getPedidoById(int idPedido);

}
