package com.concretepage.service;

import java.util.List;

import com.concretepage.entity.CotizacionDetalle;

public interface ICotizaDetService {
	List<CotizacionDetalle> getCotizacion(int idPedido);
	String updateConsolidado(String arr);
}
