package com.concretepage.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.concretepage.service.IPedidoService;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.concretepage.entity.Pedido;


@Controller
@RequestMapping("pedido")
public class PedidoController {
	@Autowired
	private IPedidoService pedidoService;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="list", method=RequestMethod.GET)
	public @ResponseBody String getPedidos() throws JsonGenerationException, JsonMappingException, IOException{
		List<Pedido> listPedidos = pedidoService.getAllPedidos();		
		JSONObject obj = new JSONObject();
		JSONArray dataJson = new JSONArray();
		
		for(Pedido pedido : listPedidos) {
			JSONArray pedidoJson = new JSONArray();
			pedidoJson.add(pedido.getCodPedido());
			pedidoJson.add(pedido.getIdPedido());
			
			if (pedido.getArea() == 10){
				pedidoJson.add("Gerencia General");
			} else {
				pedidoJson.add("Gerencia de Desarrollo Urbano");
			}
			
			if(pedido.getUsuario()==8) {
				pedidoJson.add("Paola Diaz");
			}else {
				pedidoJson.add("Orlando Denegri");
			}
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String fecha_  = dateFormat.format(pedido.getFechaRegistro());
			pedidoJson.add(fecha_);
						
			if(pedido.getEstado() == 2) {
				pedidoJson.add("Cotizado");
			}
			
			dataJson.add(pedidoJson);
		}
		obj.put("recordsTotal", listPedidos.size());
		obj.put("recordsFiltered", listPedidos.size());
		obj.put("data", dataJson);
						
		return obj.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping("cabeceraPedido")
	public @ResponseBody String getPedido(@RequestParam("pedido_id") Integer id) throws JsonGenerationException, JsonMappingException, IOException{
		List<Pedido> listPedidos = pedidoService.getAllPedido(id);		
		JSONObject obj = new JSONObject();
		JSONArray dataJson = new JSONArray();
		String usuarioDescripcion="";
		
		for(Pedido pedido : listPedidos) {
			JSONArray pedidoJson = new JSONArray();
			pedidoJson.add(pedido.getCodPedido());
			pedidoJson.add(pedido.getIdPedido());
			if(pedido.getArea()==10) {
				pedidoJson.add("Gerencia General");	
			}else {
				pedidoJson.add("Gerencia de Desarrollo Urbano");
			}
			if(pedido.getUsuario()==8) {
				pedidoJson.add("Paola Diaz");
			}else {
				pedidoJson.add("Orlando Denegri");
			}
			
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String fecha_  = dateFormat.format(pedido.getFechaRegistro());
			pedidoJson.add(fecha_);			
			
			pedidoJson.add(pedido.getEstado());			
			dataJson.add(pedidoJson);
		}
		obj.put("recordsTotal", listPedidos.size());
		obj.put("recordsFiltered", listPedidos.size());
		obj.put("data", dataJson);
						
		return obj.toJSONString();
	}
	
}
