package com.concretepage.controller;

import java.io.IOException;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.concretepage.entity.CotizacionDetalle;
import com.concretepage.service.ICotizaDetService;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

@Controller
@RequestMapping("cotizacion")
public class CotizacionController {
	@Autowired
	private ICotizaDetService cotser;
	//double min = Double.MAX_VALUE;
	@SuppressWarnings("unchecked")
	@GetMapping("cotidet")
	public @ResponseBody String getCotizacion(@RequestParam("pedido_id") Integer id)throws JsonGenerationException, JsonMappingException, IOException {		
		List<CotizacionDetalle>cotd=cotser.getCotizacion(id);
		JSONObject obj = new JSONObject();
		JSONArray dataJson = new JSONArray();
		double valor = 0;
		double min = Double.MAX_VALUE;
		for(CotizacionDetalle cd : cotd) {
			JSONArray pedidoJson = new JSONArray();
			valor = cd.getCantidad() * cd.getValorVenta();
			if(valor < min){
				min=valor;
				pedidoJson.add(cd.getIdCotizacionDetalle());
				pedidoJson.add(cd.getCotizacion().getNumCotizacion());
				pedidoJson.add(cd.getPedetalle().getBien().getDescripcion());
				pedidoJson.add(cd.getCotizacion().getProveedor().getRazonSocial());
				pedidoJson.add(min);
				dataJson.add(pedidoJson);
			}
			
		}
		obj.put("recordsTotal", dataJson.size());
		obj.put("recordsFiltered", dataJson.size());
		obj.put("data", dataJson);
						
		return obj.toJSONString();
		
	}
	
	@SuppressWarnings("unchecked")
	@PostMapping("conso")
	public String updateConsolidado(@RequestParam("id_conso") String ids) {
		JSONObject obj = new JSONObject();
		String message_ = cotser.updateConsolidado(ids);
		obj.put("messsage", message_);
		
		return obj.toJSONString();
	}

} 
