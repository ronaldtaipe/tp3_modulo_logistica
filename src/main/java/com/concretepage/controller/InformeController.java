package com.concretepage.controller;

import java.io.IOException;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.concretepage.entity.Informe;
import com.concretepage.entity.InformeDetalle;
import com.concretepage.service.IInformeService;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

@Controller
@RequestMapping("informe")
public class InformeController {
	@Autowired
	private IInformeService cotser;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="list", method=RequestMethod.GET)
	public @ResponseBody String getPedidos(@RequestParam("solicitud") String solicitud,@RequestParam("responsable") String responsable) throws JsonGenerationException, JsonMappingException, IOException{
		List<Informe> listPedidos = cotser.getAllInforme(solicitud, responsable);	
		JSONObject obj = new JSONObject();
		JSONArray dataJson = new JSONArray();
		
		for(Informe pedido : listPedidos) {
			
			
			JSONArray pedidoJson = new JSONArray();
		pedidoJson.add(pedido.getIdInforme());
		pedidoJson.add(pedido.getBien().getCodItem());	
		pedidoJson.add(pedido.getBien().getDescripcion());
		pedidoJson.add(pedido.getBien().getMarca());
		pedidoJson.add(pedido.getCosto());
		pedidoJson.add(pedido.getMoneda());	
		
			
			dataJson.add(pedidoJson);
		}
		obj.put("recordsTotal", listPedidos.size());
		obj.put("recordsFiltered", listPedidos.size());
		obj.put("data", dataJson);
						
		return obj.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="listdetalle", method=RequestMethod.GET)
	public @ResponseBody String getPedidos(@RequestParam("informe") String informe) throws JsonGenerationException, JsonMappingException, IOException{
		List<InformeDetalle> listPedidos = cotser.getAllInformeDetalle(informe);
		System.out.println(listPedidos.size()+"CANTIDA");
		JSONObject obj = new JSONObject();
		JSONArray dataJson = new JSONArray();
		
		for(InformeDetalle pedido : listPedidos) {
			
			
	    JSONArray pedidoJson = new JSONArray();
		pedidoJson.add(pedido.getIdInformeDetalle());
		pedidoJson.add(pedido.getInforme().getBien().getDescripcion());	
		pedidoJson.add(pedido.getObservacion());
			dataJson.add(pedidoJson);
		}
		obj.put("recordsTotal", listPedidos.size());
		obj.put("recordsFiltered", listPedidos.size());
		obj.put("data", dataJson);
						
		return obj.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="insertdetalle", method=RequestMethod.GET)
	public @ResponseBody String nuevoObservacion(@RequestParam("informe") String informe,@RequestParam("observacion") String observacion) throws JsonGenerationException, JsonMappingException, IOException{
		Informe beanInforme = cotser.getInforme(Integer.parseInt(informe));
		System.out.println(beanInforme.getBien()+"BIEN");
		int respuesta = cotser.insertInformeDetalle(beanInforme, observacion);
		JSONObject obj = new JSONObject();
		obj.put("exito", respuesta);
		return obj.toJSONString();
	}
	
	
} 
