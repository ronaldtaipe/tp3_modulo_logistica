package com.concretepage.controller;

import java.io.IOException;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.concretepage.entity.Guia;
import com.concretepage.service.IGuiaRemisionService;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

@Controller
@RequestMapping("guiaremision")
public class GuiaRemisionController {
	@Autowired
	private IGuiaRemisionService cotser;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="list", method=RequestMethod.GET)
	public @ResponseBody String getPedidos(@RequestParam("item") String item,@RequestParam("area") String area,@RequestParam("orden") String orden,@RequestParam("guia") String guia,@RequestParam("solicitante") String solicitante,@RequestParam("tipodocumento") String tipodocumento,@RequestParam("fecha") String fecha) throws JsonGenerationException, JsonMappingException, IOException{
		List<Guia> listPedidos = cotser.getAllGuiaRemision(item,area,orden,guia,solicitante,tipodocumento,fecha);	
		JSONObject obj = new JSONObject();
		JSONArray dataJson = new JSONArray();
		
		for(Guia pedido : listPedidos) {
			JSONArray pedidoJson = new JSONArray();
		pedidoJson.add(pedido.getBien().getCodItem());
		pedidoJson.add(pedido.getNumeroOrden());
		pedidoJson.add(pedido.getArea().getDescripcion());
		pedidoJson.add(pedido.getSolicitante());	
		pedidoJson.add(pedido.getFechaDocumento());
		pedidoJson.add(pedido.getFechaRegistro());
		pedidoJson.add(pedido.getNumeroDocumento());
	   pedidoJson.add(pedido.getCantidad());	
		pedidoJson.add(pedido.getBien().getUnidadMedida());
		pedidoJson.add(pedido.getAlmacen());
		pedidoJson.add(pedido.getUbicacion());
			
			
			dataJson.add(pedidoJson);
		}
		obj.put("recordsTotal", listPedidos.size());
		obj.put("recordsFiltered", listPedidos.size());
		obj.put("data", dataJson);
						
		return obj.toJSONString();
	}
	
} 
