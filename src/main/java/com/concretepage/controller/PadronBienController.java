package com.concretepage.controller;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.concretepage.entity.Pedido;
import com.concretepage.entity.PedidoDetalle;
import com.concretepage.entity.listas.Grafico;
import com.concretepage.service.IPadronBienService;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;



@Controller
@RequestMapping("padronBien")
public class PadronBienController {
	private static final int INACTIVE_STATE = 0;
	private static final int ACTIVE_STATE = 1;
	
	@Autowired
	IPadronBienService padservice;
	@SuppressWarnings("unchecked")
	@GetMapping("padron")
	public @ResponseBody String getPadron(@RequestParam("fecha_ini") String fechaEntregaIni,@RequestParam("fecha_fin") String fechaEntregaFin,@RequestParam("estado") Integer estado)throws Exception {
		List<PedidoDetalle>det =padservice.getPedidoById(fechaEntregaIni, fechaEntregaFin,estado);
		JSONObject obj = new JSONObject();
		JSONArray dataJson = new JSONArray();
		
		for(PedidoDetalle detPedido : det) {
			JSONArray pedidoDetJson = new JSONArray();
			pedidoDetJson.add(detPedido.getBien().getCodItem());
			pedidoDetJson.add(detPedido.getBien().getIdBien());			
			pedidoDetJson.add(detPedido.getBien().getDescripcion());
			pedidoDetJson.add(detPedido.getBien().getCategoria());
			
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String fecha_  = dateFormat.format(detPedido.getPedido().getFechaEntrega());
			pedidoDetJson.add(fecha_);
			
			pedidoDetJson.add( (detPedido.getBien().getEstado() == ACTIVE_STATE) ? "Activo" : "Inactivo" );
			dataJson.add(pedidoDetJson);
		}
		obj.put("recordsTotal", det.size());
		obj.put("recordsFiltered", det.size());
		obj.put("data", dataJson);
		return obj.toJSONString();
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value="grafico", method=RequestMethod.GET)
	public @ResponseBody String grafico() throws Exception{
		List<Grafico> padron = padservice.getBienGraficos();
		System.out.println("--------"+padron);
		// JSONObject obj = new JSONObject();
		JSONArray dataJson = new JSONArray();
		
		int index = 0;
		for(Grafico graphic : padron) {
			JSONArray objIntern = new JSONArray();
			objIntern.add(graphic.getAreaDesc());
			objIntern.add( Integer.parseInt(graphic.getIdConta()));
			objIntern.add("false");
			if (index == 0) {
				objIntern.add("true");
			} else {
				objIntern.add("false");
			}
			dataJson.add(objIntern);
			index++;
		}		
		
		return dataJson.toJSONString();
		
	}
	

}
