package com.concretepage.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class IndexController {
	
	@RequestMapping("/")
    public String index(){
        System.out.println("Looking in the index controller.........");
        return "index.html";
    }
	
}
