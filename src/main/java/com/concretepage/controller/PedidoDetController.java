package com.concretepage.controller;



import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.concretepage.entity.PedidoDetalle;
import com.concretepage.service.IPedidoDetService;

@Controller
@RequestMapping("pedido")
public class PedidoDetController {
	@Autowired
	IPedidoDetService detservice;
	
	@SuppressWarnings("unchecked")
	@GetMapping("detpedido")
	public @ResponseBody String getPedidoDet(@RequestParam("pedido_id") Integer id) {
		List<PedidoDetalle>det =detservice.getPedidoById(id);
		JSONObject obj = new JSONObject();
		JSONArray dataJson = new JSONArray();
		
		for(PedidoDetalle detPedido : det) {
			JSONArray pedidoDetJson = new JSONArray();
			pedidoDetJson.add(detPedido.getBien().getCodItem());
			pedidoDetJson.add(detPedido.getBien().getIdBien());			
			pedidoDetJson.add(detPedido.getBien().getDescripcion());
			pedidoDetJson.add(detPedido.getBien().getMarca());
			pedidoDetJson.add(detPedido.getCantidad());
			pedidoDetJson.add("1");			
			dataJson.add(pedidoDetJson);
		}
		obj.put("recordsTotal", det.size());
		obj.put("recordsFiltered", det.size());
		obj.put("data", dataJson);
		return obj.toJSONString();
	}
	
	
}
